default: build

build:
    emacs --quick --batch --eval \
      '(progn \
         (add-to-list (quote load-path) (expand-file-name (format "straight/build-%s/yasnippet" emacs-major-version) user-emacs-directory)) \
         (require (quote yasnippet)) \
         (let ((yas-snippet-dirs (list "."))) \
           (yas-recompile-all)))'

clean:
    rm --force **/.yas-compiled-snippets.el
